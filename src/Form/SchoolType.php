<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => false, ])
            ->add('address', TextType::class, [
                'required' => false, ])
            ->add('Maths', CheckboxType::class, [
                'required' => false, ])
            ->add('Algebra', CheckboxType::class, [
                'required' => false, ])
            ->add('Geometry', CheckboxType::class, [
                'required' => false, ])
            ->add('Science', CheckboxType::class, [
                'required' => false, ])
            ->add('Biology', CheckboxType::class, [
                'required' => false, ])
            ->add('Physics', CheckboxType::class, [
                'required' => false, ])
            ->add('Chemistry', CheckboxType::class, [
                'required' => false, ])
            ->add('Geography', CheckboxType::class, [
                'required' => false, ])
            ->add('History', CheckboxType::class, [
                'required' => false, ])
            ->add('Citizenship', CheckboxType::class, [
                'required' => false, ])
            ->add('PE', CheckboxType::class, [
                'required' => false, ])
            ->add('Art', CheckboxType::class, [
                'required' => false, ])
            ->add('Music', CheckboxType::class, [
                'required' => false, ])
            ->add('Technology', CheckboxType::class, [
                'required' => false, ])
            ->add('English', CheckboxType::class, [
                'required' => false, ])
            ->add('IT', CheckboxType::class, [
                'required' => false, ])
            ->add('Religious', CheckboxType::class, [
                'required' => false, ])
            ->add('IA', CheckboxType::class, [
                'required' => false, ])
            ->add('IB', CheckboxType::class, [
                'required' => false, ])
            ->add('IC', CheckboxType::class, [
                'required' => false, ])
            ->add('ID', CheckboxType::class, [
                'required' => false, ])
            ->add('IE', CheckboxType::class, [
                'required' => false, ])
            ->add('IF', CheckboxType::class, [
                'required' => false, ])
            ->add('IIA', CheckboxType::class, [
                'required' => false, ])
            ->add('IIB', CheckboxType::class, [
                'required' => false, ])
            ->add('IIC', CheckboxType::class, [
                'required' => false, ])
            ->add('IID', CheckboxType::class, [
                'required' => false, ])
            ->add('IIE', CheckboxType::class, [
                'required' => false, ])
            ->add('IIF', CheckboxType::class, [
                'required' => false, ])
            ->add('IIIA', CheckboxType::class, [
                'required' => false, ])
            ->add('IIIB', CheckboxType::class, [
                'required' => false, ])
            ->add('IIIC', CheckboxType::class, [
                'required' => false, ])
            ->add('IIID', CheckboxType::class, [
                'required' => false, ])
            ->add('IIIE', CheckboxType::class, [
                'required' => false, ])
            ->add('IIIF', CheckboxType::class, [
                'required' => false, ])
            ->add('IVA', CheckboxType::class, [
                'required' => false, ])
            ->add('IVB', CheckboxType::class, [
                'required' => false, ])
            ->add('IVC', CheckboxType::class, [
                'required' => false, ])
            ->add('IVD', CheckboxType::class, [
                'required' => false, ])
            ->add('IVE', CheckboxType::class, [
                'required' => false, ])
            ->add('IVF', CheckboxType::class, [
                'required' => false, ])
            ->add('VA', CheckboxType::class, [
                'required' => false, ])
            ->add('VB', CheckboxType::class, [
                'required' => false, ])
            ->add('VC', CheckboxType::class, [
                'required' => false, ])
            ->add('VD', CheckboxType::class, [
                'required' => false, ])
            ->add('VE', CheckboxType::class, [
                'required' => false, ])
            ->add('VF', CheckboxType::class, [
                'required' => false, ])
            ->add('VIA', CheckboxType::class, [
                'required' => false, ])
            ->add('VIB', CheckboxType::class, [
                'required' => false, ])
            ->add('VIC', CheckboxType::class, [
                'required' => false, ])
            ->add('VID', CheckboxType::class, [
                'required' => false, ])
            ->add('VIE', CheckboxType::class, [
                'required' => false, ])
            ->add('VIF', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIA', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIB', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIC', CheckboxType::class, [
                'required' => false, ])
            ->add('VIID', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIE', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIF', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIIA', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIIB', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIIC', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIID', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIIE', CheckboxType::class, [
                'required' => false, ])
            ->add('VIIIF', CheckboxType::class, [
                'required' => false, ])
            ->add('admin_name', TextType::class, ['label' => false])
            ->add('password', PasswordType::class, ['label' => false])
            ->add('register', SubmitType::class);
    }
}
