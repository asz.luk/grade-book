<?php

namespace App\Form\ApiForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SchoolApiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('classes', CollectionType::class, [
                'allow_add' => true,
            ])
            ->add('subjects', CollectionType::class, [
                'allow_add' => true,
            ]);
    }
}
