<?php

namespace App\Form;

use App\Entity\Student;
use App\Repository\SchoolRepository;
use App\Repository\StudentRepository;
use App\Repository\TeacherRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class GradeType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var StudentRepository
     */
    private $repository;

    /**
     * @var TeacherRepository
     */
    private $teacherRepository;

    /**
     * @var SchoolRepository
     */
    private $schoolRepository;

    /**
     * GradeType constructor.
     *
     * @param Security $security
     * @param StudentRepository $repository
     * @param TeacherRepository $teacherRepository
     * @param SchoolRepository $schoolRepository
     */
    public function __construct(Security $security, StudentRepository $repository, TeacherRepository $teacherRepository, SchoolRepository $schoolRepository)
    {
        $this->security = $security;
        $this->repository = $repository;
        $this->teacherRepository = $teacherRepository;
        $this->schoolRepository = $schoolRepository;
    }

    /**
     * Prepare students list.
     *
     * @return array
     */
    public function prepareStudentsList()
    {
        $user = $this->security->getUser();
        $school = $user->getSchool();
        $role = $user->getRoles();

        if ('ROLE_ADMIN' == $role[0]) {
            $students = $this->repository->findAllStudentsBySchool($school);
        } elseif ('ROLE_TEACHER' == $role[0]) {
            $teacherEmail = $user->getEmail();
            $teacherClass = $this->teacherRepository->findTeacherClasses($teacherEmail);

            foreach ($teacherClass[0]['classes'] as $key => $value) {
                $classes[] = $value;
            }

            $students = $this->repository->findAllStudentsBySchoolAndClass($school, $classes);
        }

        return $students;
    }

    /**
     * Prepare subjects list.
     *
     * @return array
     */
    public function prepareSubjects()
    {
        $user = $this->security->getUser();
        $school = $user->getSchool();
        $role = $user->getRoles();

        if ('ROLE_ADMIN' == $role[0]) {
            return $school->getSubjects();
        } elseif ('ROLE_TEACHER' == $role[0]) {
            $teacherEmail = $user->getEmail();
            $teacherSubjects = $this->teacherRepository->findTeacherSubjects($teacherEmail);

            foreach ($teacherSubjects[0]['subject'] as $key => $value) {
                $subjects[] = $value;
            }

            return $subjects;
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('student', EntityType::class, [
                'class' => Student::class,
                'choice_label' => function (Student $student) {
                    return sprintf('%s %s', $student->getName(), $student->getSurname());
                },
                'placeholder' => 'Choose student',
                'choices' => $this->prepareStudentsList(),
            ])
            ->add('class', ChoiceType::class, [
                'choices' => $this->prepareSubjects(),
                'choice_label' => function ($value) {
                    return $value;
                },
            ])
            ->add('result', ChoiceType::class, [
                'choices' => [
                    'niedostateczny' => 'niedostateczny',
                    'niedostateczny +' => 'niedostateczny +',
                    'dopuszczający -' => 'dopuszczający -',
                    'dopuszczający' => 'dopuszczający',
                    'dopuszczający +' => 'dopuszczający +',
                    'dostateczny -' => 'dostateczny -',
                    'dostateczny' => 'dostateczny',
                    'dostateczny +' => 'dostateczny +',
                    'dobry -' => 'dobry -',
                    'dobry' => 'dobry',
                    'dobry +' => 'dobry +',
                    'bardzo dobry -' => 'bardzo dobry -',
                    'bardzo dobry' => 'bardzo dobry',
                    'bardzo dobry +' => 'bardzo dobry +',
                    'celujący -' => 'celujący -',
                    'celujący' => 'celujący',
                    'celujący +' => 'celujący +',
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'test' => 'test',
                    'exam' => 'exam',
                    'answer' => 'answer',
                    'colloquium' => 'colloquium',
                    'homework' => 'homework',
                    'paper' => 'paper',
                ],
            ])
            ->add('date', DateType::class)
            ->add('save', SubmitType::class);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\Entity\Grade']);
    }
}
