<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class EditTeacherType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $school = $user->getSchool();
        $schoolSubjects = $school->getSubjects();
        $schoolClasses = $school->getClasses();

        foreach ($schoolSubjects as $key => $value) {
            $subjects[$value] = $value;
        }

        foreach ($schoolClasses as $key => $value) {
            $classes[$value] = $value;
        }

        $builder
            ->add('name', TextType::class, ['label' => false])
            ->add('surname', TextType::class, ['label' => false])
            ->add('subject', ChoiceType::class, [
                'choices' => $subjects,
                'label' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('classes', ChoiceType::class, [
                'choices' => $classes,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('save', SubmitType::class);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\Entity\Teacher']);
    }
}
