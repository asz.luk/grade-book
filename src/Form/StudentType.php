<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class StudentType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->security->getUser();
        $school = $user->getSchool();
        $schoolClasses = $school->getClasses();

        foreach ($schoolClasses as $key => $value) {
            $classes[$value] = $value;
        }

        $builder
            ->add('name', TextType::class, ['label' => false])
            ->add('surname', TextType::class, ['label' => false])
            ->add('class', ChoiceType::class, [
                'choices' => $classes,
            ])
            ->add('email', TextType::class, ['label' => false])
            ->add('password', PasswordType::class, ['label' => false])
            ->add('address', TextType::class, ['label' => false])
            ->add('city', TextType::class, ['label' => false])
            ->add('postalcode', TextType::class, ['label' => false])
            ->add('phone', TextType::class, ['label' => false])
            ->add('save', SubmitType::class);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'App\Entity\Student']);
    }
}
