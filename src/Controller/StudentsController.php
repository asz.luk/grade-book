<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Student;
use App\Form\EditStudentType;
use App\Form\StudentType;
use App\Model\StudentModel;
use App\Model\UserModel;
use App\Repository\StudentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class StudentsController extends AbstractController
{
    /**
     * @var StudentModel
     */
    private $studentModel;

    /**
     * @var StudentRepository
     */
    private $studentRepository;

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var Security
     */
    private $security;

    /**
     * StudentsController constructor.
     */
    public function __construct(StudentModel $studentModel, StudentRepository $studentRepository, UserModel $userModel, Security $security)
    {
        $this->studentModel = $studentModel;
        $this->userModel = $userModel;
        $this->security = $security;
        $this->studentRepository = $studentRepository;
    }

    /**
     * Rendering students main page.
     *
     * @Route("/students", name="students")
     */
    public function renderStudentPage()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Students';

        $user = $this->security->getUser();
        $school = $user->getSchool();
        $students = $school->getStudents();

        return $this->render('dashboard/students.twig', [
            'students' => $students,
            'title' => $title,
        ]);
    }

    /**
     * Adding student.
     *
     * @Route("/add-student", name="add-student")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addStudent(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Add Student';

        $form = $this->createForm(StudentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $admin = $this->security->getUser();
            $school = $admin->getSchool();

            $user = $this->userModel->prepareStudent($form->getData(), $school);
            $this->userModel->save($user);

            $student = $this->studentModel->prepareStudent($form->getData(), $school);
            $this->studentModel->save($student);

            return $this->redirectToRoute('students');
        }

        return $this->render('dashboard/add-student.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Editing student data.
     *
     * @Route("/edit-student/{id}", name="edit-student")
     *
     * @param Request $request
     * @param $id Student
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editStudent(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Edit Student';

        $student = $this->studentRepository->find($id);

        $form = $this->createForm(EditStudentType::class, $student);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->studentModel->save($student);

            return $this->redirectToRoute('students');
        }

        return $this->render('dashboard/edit-student.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Removing student.
     *
     * @Route("/removeStudent/{id}", name="remove-student")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeStudent($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $teacher = $this->studentRepository->find($id);

        $this->studentModel->remove($teacher);

        return $this->redirectToRoute('students');
    }
}
