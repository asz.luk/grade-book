<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Form\ApiForm\SchoolApiType;
use App\Model\SchoolModel;
use App\Repository\SchoolRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SchoolApiController extends AbstractController
{
    /**
     * @var SchoolRepository
     */
    private $repository;

    /**
     * @var SchoolModel
     */
    private $model;

    /**
     * SchoolApiController constructor.
     * @param SchoolRepository $repository
     * @param SchoolModel $model
     */
    public function __construct(SchoolRepository $repository, SchoolModel $model)
    {
        $this->repository = $repository;
        $this->model = $model;
    }

    /**
     * @Route("getSchools", name = "get_schools", methods = {"GET"}, requirements = { "_format" = "json" })
     */
    public function getSchoolsAction(): JsonResponse
    {
        $schools = $this->repository->findAll();

        return $this->json($schools, 200, [], [
            'groups' => ['school'],
        ]);
    }

    /**
     * @Route("getSchool/{id}", name = "get_school", methods = {"GET"}, requirements = {"_format" = "json"})
     *
     * @param $id
     * @return JsonResponse
     */
    public function getSchoolAction($id): JsonResponse
    {
        $school = $this->repository->findOneById($id);

        if (!$school) {
            return $this->json([
                'errors' => ['School not found'],
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($school, 200, [], [
            'groups' => ['school'],
        ]);
    }

    /**
     * @Route("createSchool", name = "create_school", methods = {"POST"}, requirements={"_format" = "json"})
     */
    public function createSchoolAction(Request $request): JsonResponse
    {
        $data = \json_decode($request->getContent(), true);

        $form = $this->createForm(SchoolApiType::class);
        $form->submit($data);

        $school = $this->model->prepareApiSchool($form->getData());
        $this->model->save($school);

        return $this->json(null, Response::HTTP_CREATED, [
            'Location' => $this->generateUrl('get_school', [
                'id' => $school->getId(),
            ]),
        ]);
    }

    /**
     * @Route("/updateSchool/{id}", name = "update_school", methods = {"PUT"}, requirements = { "_format" = "json" })
     *
     * @param $id
     * @return JsonResponse
     */
    public function updateSchoolAction(Request $request, $id): JsonResponse
    {
        $school = $this->repository->findOneById($id);

        if (!$school) {
            return $this->json([
                'errors' => ['School not found'],
            ], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SchoolApiType::class, $school);

        $data = \json_decode($request->getContent(), true);

        $form->submit($data);

        $this->model->save($school);

        return $this->json($school);
    }

    /**
     * @Route("/removeSchool/{id}", name = "remove_school", methods = {"DELETE"}, requirements = { "_format" = "json" })
     *
     * @param $id
     * @return JsonResponse
     */
    public function removeSchoolAction($id): JsonResponse
    {
        $school = $this->repository->findOneById($id);

        if (!$school) {
            return $this->json([
                'errors' => ['School not found'],
            ], Response::HTTP_NOT_FOUND);
        }

        $this->model->remove($school);

        $msg = 'Resource deleted successfully';

        return $this->json($msg, 200);
    }
}
