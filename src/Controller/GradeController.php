<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\GradeType;
use App\Model\GradeModel;
use App\Repository\GradeRepository;
use App\Repository\StudentRepository;
use App\Repository\TeacherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class GradeController extends AbstractController
{
    /**
     * @var GradeModel
     */
    private $gradeModel;

    /**
     * @var TeacherRepository
     */
    private $teacherRepository;

    /**
     * @var StudentRepository
     */
    private $studentRepository;

    /**
     * @var GradeRepository
     */
    private $gradeRepository;

    /**
     * GradeController constructor.
     */
    public function __construct(GradeModel $gradeModel, Security $security, TeacherRepository $teacherRepository, StudentRepository $studentRepository, GradeRepository $gradeRepository)
    {
        $this->gradeModel = $gradeModel;
        $this->security = $security;
        $this->teacherRepository = $teacherRepository;
        $this->studentRepository = $studentRepository;
        $this->gradeRepository = $gradeRepository;
    }

    /**
     * @var Security
     */
    private $security;

    /**
     * Rendering grades main page.
     *
     * @Route("/grades", name="grades")
     */
    public function renderGradesPage()
    {
        $title = 'Grades';

        $user = $this->security->getUser();
        $role = $user->getRoles();

        if ('ROLE_ADMIN' == $role[0]) {
            $school = $user->getSchool();
            $grades = $school->getGrades();
        } elseif ('ROLE_TEACHER' == $role[0]) {
            $teacherEmail = $user->getEmail();
            $teacherId = $this->teacherRepository->findTeacherByEmail($teacherEmail);
            $teacher = $this->teacherRepository->find($teacherId[0]['id']);
            $grades = $teacher->getGrades();
        } elseif ('ROLE_STUDENT' == $role[0]) {
            $studentEmail = $user->getEmail();
            $studentId = $this->studentRepository->findStudentByEmail($studentEmail);
            $grades = $this->gradeRepository->findAllStudentGrades($studentId[0]['id']);
        }

        return $this->render('dashboard/grades.twig', [
            'grades' => $grades,
            'title' => $title,
        ]);
    }

    /**
     * Adding grade.
     *
     * @Route("/add-grade", name="add-grade")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addGrade(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        $title = 'Add Grade';

        $form = $this->createForm(GradeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->security->getUser();
            $school = $user->getSchool();
            $teacherEmail = $user->getEmail();
            $teacherId = $this->teacherRepository->findTeacherByEmail($teacherEmail);
            $teacher = $this->teacherRepository->find($teacherId[0]['id']);

            $grade = $this->gradeModel->prepareGrade($form->getData(), $teacher, $school);
            $this->gradeModel->save($grade);

            return $this->redirectToRoute('grades');
        }

        return $this->render('dashboard/add-grade.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Editing grade data.
     *
     * @Route("/edit-grade/{id}", name="edit-grade")
     *
     * @param Request $request
     * @param $id Grade
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editGrade(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        $title = 'Edit Grade';

        $grade = $this->gradeRepository->find($id);

        $form = $this->createForm(GradeType::class, $grade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->gradeModel->save($grade);

            return $this->redirectToRoute('grades');
        }

        return $this->render('dashboard/edit-grade.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Removing grade.
     *
     * @Route("/removeGrade/{id}", name="remove-grade")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeGrade($id)
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');

        $grade = $this->gradeRepository->find($id);

        $this->gradeModel->remove($grade);

        return $this->redirectToRoute('grades');
    }
}
