<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * Displays the home page.
     *
     * @Route("/dashboard", name="dashboard")
     */
    public function showDashboard()
    {
        $title = 'Dashboard';

        return $this->render('dashboard/dashboard.twig', [
            'title' => $title,
        ]);
    }
}
