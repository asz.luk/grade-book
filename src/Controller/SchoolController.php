<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\SchoolType;
use App\Model\SchoolModel;
use App\Model\UserModel;
use App\Repository\SchoolRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SchoolController extends AbstractController
{
    /**
     * @var SchoolModel
     */
    private $schoolModel;

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * SchoolController constructor.
     * @param SchoolModel $schoolModel
     * @param UserModel $userModel
     */
    public function __construct(SchoolModel $schoolModel, UserModel $userModel)
    {
        $this->schoolModel = $schoolModel;
        $this->userModel = $userModel;
    }

    /**
     * Registering school.
     *
     * @Route("/register", name="register")
     *
     * @param Request $request
     * @param SchoolRepository $repository
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerSchool(Request $request, SchoolRepository $repository)
    {
        $form = $this->createForm(SchoolType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $school = $this->schoolModel->prepareSchool($form->getData());
            $this->schoolModel->save($school);

            $admin = $this->userModel->prepareAdmin($form->getData(), $school);
            $this->userModel->save($admin);

            return $this->redirectToRoute('app_login');
        }

        return $this->render('register.twig', [
            'form' => $form->createView(),
        ]);
    }
}
