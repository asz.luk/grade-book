<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Teacher;
use App\Form\EditTeacherType;
use App\Form\TeacherType;
use App\Model\TeacherModel;
use App\Model\UserModel;
use App\Repository\TeacherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class TeachersController extends AbstractController
{
    /**
     * @var TeacherModel
     */
    private $teacherModel;

    /**
     * @var TeacherRepository
     */
    private $teacherRepository;

    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * @var Security
     */
    private $security;

    /**
     * TeachersController constructor.
     */
    public function __construct(TeacherModel $teacherModel, TeacherRepository $teacherRepository, UserModel $userModel, Security $security)
    {
        $this->teacherModel = $teacherModel;
        $this->userModel = $userModel;
        $this->security = $security;
        $this->teacherRepository = $teacherRepository;
    }

    /**
     * Rendering students main page.
     *
     * @Route("/teachers", name="teachers")
     */
    public function renderTeachersPage()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Teachers';

        $user = $this->security->getUser();
        $school = $user->getSchool();
        $teachers = $school->getTeachers();

        return $this->render('dashboard/teachers.twig', [
            'teachers' => $teachers,
            'title' => $title,
        ]);
    }

    /**
     * Adding teacher.
     *
     * @Route("/add-teacher", name="add-teacher")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addTeacher(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Add Teacher';

        $form = $this->createForm(TeacherType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $admin = $this->security->getUser();
            $school = $admin->getSchool();

            $teacher = $this->teacherModel->prepareTeacher($form->getData(), $school);
            $this->teacherModel->save($teacher);

            $user = $this->userModel->prepareTeacher($form->getData(), $school);
            $this->userModel->save($user);

            return $this->redirectToRoute('teachers');
        }

        return $this->render('dashboard/add-teacher.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Editing teacher data.
     *
     * @Route("/edit-teacher/{id}", name="edit-teacher")
     *
     * @param $id Teacher
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editTeacher(Request $request, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $title = 'Edit Teacher';

        $teacher = $this->teacherRepository->find($id);

        $form = $this->createForm(EditTeacherType::class, $teacher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->teacherModel->save($teacher);

            return $this->redirectToRoute('teachers');
        }

        return $this->render('dashboard/edit-teacher.twig', [
            'title' => $title,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Removing teacher.
     *
     * @Route("/removeTeacher/{id}", name="remove-teacher")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeTeacher($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $teacher = $this->teacherRepository->find($id);

        $this->teacherModel->remove($teacher);

        return $this->redirectToRoute('teachers');
    }
}
