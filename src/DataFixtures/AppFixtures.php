<?php

namespace App\DataFixtures;

use App\Entity\School;
use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $schoolNames = ['Home.pl', 'Szkoła Podstawowa nr. 48', 'Liceum Ogólnokształcące nr. 6'];
        $schoolAddresses = ['Zbożowa 4,  70-653 Szczecin', 'Czorsztyńska 35, 71-201 Szczecin', 'Jagiellońska 41, 70-382 Szczecin'];
        $schoolClasses = [['IA', 'IIA', 'IIIA', 'IVA'], ['IB', 'IIB', 'IIIB', 'IVB'], ['IC', 'IIC', 'IIIC', 'IVC']];
        $schoolSubjects = [['Technology', 'English', 'IT'], ['Maths', 'Biology', 'Physics'], ['Chemistry', 'History', 'Art']];

        $adminEmails = ['home@luke.net.pl', 'sp48@luke.net.pl', 'lo6@luke.net.pl'];
        $adminRole = ['ROLE_ADMIN'];
        $adminPassword = 'Kombat01#';

        $teacherNames = [['Elzbieta', 'Aleksandra', 'Marcelina'], ['Dorian', 'Eustachy', 'Zuzanna'], ['Zuza', 'Bartlomiej', 'Ernest']];
        $teacherSurnames = [['Cieslak', 'Brzezinska', 'Kazmierczak'], ['Bak', 'Wlodarczyk', 'Tomaszewska'], ['Kolodziej', 'Wysocki', 'Sobczak']];
        $teacherSubjects = [[['Technology'], ['English'], ['IT']], [['Maths'], ['Biology'], ['Physics']], [['Chemistry'], ['History'], ['Art']]];
        $teacherRole = ['ROLE_TEACHER'];

        $studentNames = [['Antonina', 'Anastazja', 'Magda', 'Julita'], ['Elwira', 'Gniewomir', 'Daniel', 'Roman'], ['Karol', 'Olgierd', 'Michał', 'Kamila']];
        $studentSurnames = [['Wasilewska', 'Kalinowska', 'Szczepanska', 'Czarnecka'], ['Jankowska', 'Walczak', 'Wisniewski', 'Jankowski'], ['Wasilewski', 'Kucharski', 'Brzęczyszczykiewicz', 'Pospolita']];
        $studentRole = ['ROLE_STUDENT'];

        $next = 0;

        for ($i = 0; $i < 3; ++$i) {
            $school = new School();
            $school->setName($schoolNames[$i]);
            $school->setAddress($schoolAddresses[$i]);
            $school->setClasses($schoolClasses[$i]);
            $school->setSubjects($schoolSubjects[$i]);
            $manager->persist($school);

            $user = new User();
            $user->setEmail($adminEmails[$i]);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $adminPassword));
            $user->setRoles($adminRole);
            $user->setSchool($school);
            $manager->persist($user);

            for ($t = 0; $t < 3; ++$t) {
                $teacher = new Teacher();
                $teacher->setName($teacherNames[$i][$t]);
                $teacher->setSurname($teacherSurnames[$i][$t]);
                $teacher->setSubject($teacherSubjects[$i][$t]);
                $teacher->setClasses($schoolClasses[$t]);
                $teacher->setEmail(strtolower($teacherNames[$i][$t]).'.'.strtolower($teacherSurnames[$i][$t]).'@'.'luke.net.pl');
                $teacher->setSchool($school);
                $manager->persist($teacher);

                $teacherUser = new User();
                $teacherUser->setEmail(strtolower($teacherNames[$i][$t]).'.'.strtolower($teacherSurnames[$i][$t]).'@'.'luke.net.pl');
                $teacherUser->setPassword($this->passwordEncoder->encodePassword($teacherUser, $adminPassword));
                $teacherUser->setRoles($teacherRole);
                $teacherUser->setSchool($school);
                $manager->persist($teacherUser);
            }

            for ($s = 0; $s < 4; ++$s) {
                $student = new Student();
                $student->setName($studentNames[$i][$s]);
                $student->setSurname($studentSurnames[$i][$s]);
                $student->setClass($schoolClasses[$i][$s]);
                $student->setEmail('student'.$next.'@'.'luke.net.pl');
                $student->setAddress('Zbożowa 4');
                $student->setCity('Szczecin');
                $student->setPostalcode('70-653');
                $student->setPhone(600000000);
                $student->setSchool($school);
                $manager->persist($student);

                $studentUser = new User();
                $studentUser->setEmail('student'.'_'.$next.'@'.'luke.net.pl');
                $studentUser->setPassword($this->passwordEncoder->encodePassword($user, $adminPassword));
                $studentUser->setRoles($studentRole);
                $studentUser->setSchool($school);
                $manager->persist($studentUser);
                ++$next;
            }
        }
        $manager->flush();
    }
}
