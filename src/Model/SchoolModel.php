<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\School;
use Doctrine\ORM\EntityManagerInterface;

class SchoolModel
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new school object.
     *
     * @param array $data Data of school
     *
     * @return School
     */
    public function prepareSchool(array $data): School
    {
        $school = new School();

        $school->setName($data['name']);
        $school->setAddress($data['address']);
        $school->setSubjects($this->checkSubjects($data));
        $school->setClasses($this->checkClasses($data));

        return $school;
    }

    /**
     * Creates a new school object by API.
     *
     * @param array $data
     *
     * @return School
     */
    public function prepareApiSchool(array $data): School
    {
        $school = new School();

        $school->setName($data['name']);
        $school->setAddress($data['address']);
        $school->setSubjects($data['subjects']);
        $school->setClasses($data['classes']);

        return $school;
    }

    /**
     * Save school object in database.
     *
     * @param School $school School Object
     *
     * @return void
     */
    public function save($school)
    {
        $entityManager = $this->entityManager;
        $entityManager->persist($school);
        $entityManager->flush();
    }

    /**
     * Remove school object from database.
     *
     * @param $school
     */
    public function remove($school)
    {
        $entityManager = $this->entityManager;
        $entityManager->remove($school);
        $entityManager->flush();
    }

    /**
     * Finds school subjects.
     *
     * @param array $data Data of school
     *
     * @return array $subjects School subjects
     */
    public function checkSubjects(array $data): array
    {
        foreach ($data as $key => $value) {
            if (true == $value && 'name' != $key && 'address' != $key && 'admin_name' != $key && 'password' != $key && $key == preg_match('/[I|II|III|IV|V|VI|VII|VIII]+[A-F]/', $key)) {
                $subjects[] = $key;
            }
        }

        return $subjects;
    }

    /**
     * Finds school classes.
     *
     * @param array $data Data of school
     *
     * @return array $classes School classes
     */
    public function checkClasses(array $data): array
    {
        foreach ($data as $key => $value) {
            if (true == $value && $key != preg_match('/[I|II|III|IV|V|VI|VII|VIII]+[A-F]/', $key)) {
                $classes[] = $key;
            }
        }

        return $classes;
    }
}
