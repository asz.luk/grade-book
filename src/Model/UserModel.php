<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserModel
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserModel constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * Creates admin account for school.
     *
     * @param array $data Data of school
     *
     * @return User
     */
    public function prepareAdmin($data, $school): User
    {
        $user = new User();
        $role = ['ROLE_ADMIN'];

        $user->setEmail($data['admin_name']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['password']));
        $user->setRoles($role);
        $user->setSchool($school);

        return $user;
    }

    /**
     * Creates teacher user.
     *
     * @param array $data Data of teacher
     *
     * @return User
     */
    public function prepareTeacher($data, $school): User
    {
        $user = new User();
        $role = ['ROLE_TEACHER'];

        $user->setEmail($data['email']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['password']));
        $user->setRoles($role);
        $user->setSchool($school);

        return $user;
    }

    /**
     * Creates student user.
     *
     * @param array $data Data of student
     *
     * @return User
     */
    public function prepareStudent($data, $school): User
    {
        $user = new User();
        $role = ['ROLE_STUDENT'];

        $user->setEmail($data['email']);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['password']));
        $user->setRoles($role);
        $user->setSchool($school);

        return $user;
    }

    /**
     * Save school admin object in database.
     *
     * @param User $admin School admin
     *
     * @return void
     */
    public function save($admin)
    {
        $entityManager = $this->entityManager;
        $entityManager->persist($admin);
        $entityManager->flush();
    }
}
