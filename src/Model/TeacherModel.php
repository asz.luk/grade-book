<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Teacher;
use Doctrine\ORM\EntityManagerInterface;

class TeacherModel
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new teacher.
     *
     * @param array $data Data of teacher
     *
     * @return Teacher
     */
    public function prepareTeacher(array $data, $school): Teacher
    {
        $teacher = new Teacher();

        $teacher->setName($data['name']);
        $teacher->setSurname($data['surname']);
        $teacher->setSubject($data['subject']);
        $teacher->setClasses($data['classes']);
        $teacher->setEmail($data['email']);
        $teacher->setSchool($school);

        return $teacher;
    }

    /**
     * Save teacher object in database.
     *
     * @param Teacher $teacher Teacher Object
     *
     * @return void
     */
    public function save($teacher)
    {
        $entityManager = $this->entityManager;
        $entityManager->persist($teacher);
        $entityManager->flush();
    }

    /**
     * Remove teacher object.
     */
    public function remove($teacher)
    {
        $entityManager = $this->entityManager;
        $entityManager->remove($teacher);
        $entityManager->flush();
    }
}
