<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;

class StudentModel
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new student.
     *
     * @param array $data Data of student
     */
    public function prepareStudent(array $data, $school): Student
    {
        $student = new Student();

        $student->setName($data['name']);
        $student->setSurname($data['surname']);
        $student->setEmail($data['email']);
        $student->setClass($data['class']);
        $student->setAddress($data['address']);
        $student->setCity($data['city']);
        $student->setPostalcode($data['postalcode']);
        $phone = (int) $data['phone'];
        $student->setPhone($phone);
        $student->setSchool($school);

        return $student;
    }

    /**
     * Save student object in database.
     *
     * @param Student $student Student Object
     *
     * @return void
     */
    public function save($student)
    {
        $entityManager = $this->entityManager;
        $entityManager->persist($student);
        $entityManager->flush();
    }

    /**
     * Remove student object.
     *
     * @param $student
     */
    public function remove($student)
    {
        $entityManager = $this->entityManager;
        $entityManager->remove($student);
        $entityManager->flush();
    }
}
