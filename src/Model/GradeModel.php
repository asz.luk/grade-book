<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\Grade;
use Doctrine\ORM\EntityManagerInterface;

class GradeModel
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GradeModel constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a new grade.
     *
     * @param array $data Data of grade
     * @param $teacher
     * @param $school
     *
     * @return Grade
     */
    public function prepareGrade(array $data, $teacher, $school): Grade
    {
        $grade = new Grade();

        $grade->setStudent($data['student']);
        $grade->setClass($data['class']);
        $grade->setResult($data['result']);
        $grade->setType($data['type']);
        $grade->setDate($data['date']);
        $grade->setTeacher($teacher);
        $grade->setSchool($school);

        return $grade;
    }

    /**
     * Save grade object in database.
     *
     * @param $grade
     *
     * @return void
     */
    public function save($grade)
    {
        $entityManager = $this->entityManager;
        $entityManager->persist($grade);
        $entityManager->flush();
    }

    /**
     * Remove grade object.
     *
     * @param $grade
     *
     * @return void
     */
    public function remove($grade)
    {
        $entityManager = $this->entityManager;
        $entityManager->remove($grade);
        $entityManager->flush();
    }
}
