<?php

namespace App\Repository;

use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]    findAll()
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Student::class);
    }

    public function findAllStudentsBySchool($school): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT s FROM App\Entity\Student s WHERE s.school = :school')
            ->setParameter('school', $school);

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findAllStudentsBySchoolAndClass($school, $class): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT s FROM App\Entity\Student s WHERE s.school = :school AND s.class IN (:class)')
            ->setParameter('school', $school)
            ->setParameter('class', $class);

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findStudentByEmail($email)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT s.id FROM App\Entity\Student s WHERE s.email = :email')
            ->setParameter('email', $email);

        return $query->getResult();
    }
}
