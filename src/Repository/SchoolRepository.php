<?php

namespace App\Repository;

use App\Entity\School;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method School|null find($id, $lockMode = null, $lockVersion = null)
 * @method School|null findOneBy(array $criteria, array $orderBy = null)
 * @method School[]    findAll()
 * @method School[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, School::class);
    }

    public function findAllSchoolClasses($school): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT s.classes FROM App\Entity\School s WHERE s.id = :school')
            ->setParameter('school', $school);

        // returns an array of Product objects
        return $query->getResult();
    }
}
