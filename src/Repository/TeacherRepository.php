<?php

namespace App\Repository;

use App\Entity\Teacher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Teacher|null find($id, $lockMode = null, $lockVersion = null)
 * @method Teacher|null findOneBy(array $criteria, array $orderBy = null)
 * @method Teacher[]    findAll()
 * @method Teacher[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeacherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Teacher::class);
    }

    public function findTeacherClasses($email): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT t.classes FROM App\Entity\Teacher t WHERE t.email = :email')
            ->setParameter('email', $email);

        return $query->getResult();
    }

    public function findTeacherSubjects($email): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT t.subject FROM App\Entity\Teacher t WHERE t.email = :email')
            ->setParameter('email', $email);

        return $query->getResult();
    }

    public function findTeacherByEmail($email)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery('SELECT t.id FROM App\Entity\Teacher t WHERE t.email = :email')
            ->setParameter('email', $email);

        return $query->getResult();
    }
}
