function deleteAlertTeacher(id) {
    if (confirm("Are you sure to delete?")) {
        window.location.replace('/removeTeacher/' + id);
    }    else {
        window.location.replace('/teachers');
    }
}

function deleteAlertStudent(id) {
    if (confirm("Are you sure to delete?")) {
        window.location.replace('/removeStudent/' + id);
    }    else {
        window.location.replace('/students');
    }
}

function deleteAlertGrade(id) {
    if (confirm("Are you sure to delete?")) {
        window.location.replace('/removeGrade/' + id);
    }    else {
        window.location.replace('/grades');
    }
}
