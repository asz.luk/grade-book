# GradeBook
> Online grade book for schools.

## Table of contents.
* [Technologies](#technologies)
* [Screenshots](#screenshots)
* [Status](#status)

### Technologies
Project is created with:
* Symfony 5
* Doctrine
* PHP 7.2
* HTML / CSS / JS

### Screenshots
![alt text](public/img/read/login.png "Screenshots")

![alt text](public/img/read/register.png "Screenshots")

![alt text](public/img/read/teachers.png "Screenshots")

![alt text](public/img/read/add-grade.png "Screenshots")

![alt text](public/img/read/add-student.png "Screenshots")

![alt text](public/img/read/edit-teacher.png "Screenshots")

### Status
The project is not fully done yet.

