<?php

declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SchoolControllerTest extends WebTestCase
{
    public function testGetSchools(): void
    {
        $client = static::createClient();

        $client->xmlHttpRequest('GET', '/getSchools');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}
